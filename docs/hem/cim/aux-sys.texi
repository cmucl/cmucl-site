@comment{-*- Dictionary: /usr/lisp/scribe/hem/hem; Mode: spell; Package: Hemlock; Log: /usr/lisp/scribe/hem/hem-docs.log -*-}, , Top, Top
@node Auxiliary Systems
@chapter Auxiliary Systems
This chapter describes utilities that some implementations of @hemlock{} may
leave unprovided or unsupported.


@menu
* Key-events::
* CLX Interface::
* Slave Lisps::
* Spelling::
* File Utilities::
* Beeping::
@end menu

@node Key-events, CLX Interface, Auxiliary Systems, Auxiliary Systems
@section Key-events
@cindex I/O
@cindex keyboard input
@cindex input @subentry keyboard
@cindex mouse input
@cindex input @subentry mouse
@anchor{key-events}

These routines are defined in the @f{"EXTENSIONS"} package since other projects
have often used @hemlock{}'s input translations for interfacing to CLX.


@menu
* Introduction to Auxiliary Systems::
* Interface::
@end menu

@node Introduction to Auxiliary Systems, Interface, Key-events, Key-events
@subsection Introduction

The canonical representation of editor input is a key-event structure.  Users
can bind commands to keys (see section @ref{key-bindings}), which are non-zero
length sequences of key-events.  A key-event consists of an identifying token
known as a @i{keysym} and a field of bits representing modifiers.  Users define
keysyms, integers between 0 and 65535 inclusively, by supplying names that
reflect the legends on their keyboard's keys.  Users define modifier names
similarly, but the system chooses the bit and mask for recognizing the
modifier.  You can use keysym and modifier names to textually specify
key-events and Hemlock keys in a @f{#k} syntax.  The following are some
examples:
@example
   #k"C-u"
   #k"Control-u"
   #k"c-m-z"
   #k"control-x meta-d"
   #k"a"
   #k"A"
   #k"Linefeed"
@end example
This is convenient for use within code and in init files containing
@f{bind-key} calls.

The @f{#k} syntax is delimited by double quotes, but the system parses the
contents rather than reading it as a Common Lisp string.  Within the double
quotes, spaces separate multiple key-events.  A single key-event optionally
starts with modifier names terminated by hyphens.  Modifier names are
alphabetic sequences of characters which the system uses case-insensitively.
Following modifiers is a keysym name, which is case-insensitive if it consists
of multiple characters, but if the name consists of only a single character,
then it is case-sensitive.

You can escape special characters @dash{} hyphen, double quote, open angle
bracket, close angle bracket, and space @dash{} with a backslash, and you can
specify a backslash by using two contiguously.  You can use angle brackets to
enclose a keysym name with many special characters in it.  Between angle
brackets appearing in a keysym name position, there are only two special
characters, the closing angle bracket and backslash.



@node Interface,  , Introduction to Auxiliary Systems, Key-events
@subsection Interface

All of the following routines and variables are exported from the "EXTENSIONS"
("EXT") package.


@defun {define-keysym} {keysym preferred-name @rest{} other-names}
This function establishes a mapping from @i{preferred-name} to @i{keysym} for
purposes of @f{#k} syntax.  @i{Other-names} also map to @i{keysym}, but the
system uses @i{preferred-name} when printing key-events.  The names are
case-insensitive simple-strings; however, if the string contains a single
character, then it is used case-sensitively.  Redefining a keysym or re-using
names has undefined effects.

You can use this to define unused keysyms, but primarily this defines keysyms
defined in the @i{X Window System Protocol, MIT X Consortium Standard, X
Version 11, Release 4}.  @f{translate-key-event} uses this knowledge to
determine what keysyms are modifier keysyms and what keysym stand for
alphabetic key-events.
@end defun


@defun {define-mouse-keysym} {button keysym name shifted-bit event-key}
This function defines @i{keysym} named @i{name} for key-events representing the
X @i{button} cross the X @i{event-key} (@kwd{button-press} or
@kwd{button-release}).  @i{Shifted-bit} is a defined modifier name that
@f{translate-mouse-key-event} sets in the key-event it returns whenever the X
shift bit is set in an incoming event.

Note, by default, there are distinct keysyms for each button distinguishing
whether the user pressed or released the button.

@i{Keysym} should be an one unspecified in @i{X Window System Protocol, MIT X
Consortium Standard, X Version 11, Release 4}.
@end defun


@defun {name-keysym} {name}
This function returns the keysym named @i{name}.  If @i{name} is unknown, this
returns @nil{}.
@end defun

@defun {keysym-names} {keysym}
This function returns the list of all names for @i{keysym}.  If @i{keysym} is
undefined, this returns @nil{}.
@end defun

@defun {keysym-preferred-name} {keysym}
This returns the preferred name for @i{keysym}, how it is typically printed.
If @i{keysym} is undefined, this returns @nil{}.
@end defun

@defun {define-key-event-modifier} {long-name short-name}
This establishes @i{long-name} and @i{short-name} as modifier names for
purposes of specifying key-events in @f{#k} syntax.  The names are
case-insensitive simple-strings.  If either name is already defined, this
signals an error.

The system defines the following default modifiers (first the long name,
then the short name):
@itemize
@item
@f{"Hyper"}, @f{"H"}

@item
@f{"Super"}, @f{"S"}

@item
@f{"Meta"}, @f{"M"}

@item
@f{"Control"}, @f{"C"}

@item
@f{"Shift"}, @f{"Shift"}

@item
@f{"Lock"}, @f{"Lock"}
@end itemize
@end defun


@defvr {Variable} *all-modifier-names*
This variable holds all the defined modifier names.
@end defvr


@defun {define-clx-modifier} {clx-mask modifier-name}
This function establishes a mapping from @i{clx-mask} to a defined key-event
@i{modifier-name}.  @f{translate-key-event} and @f{translate-mouse-key-event}
can only return key-events with bits defined by this routine.

The system defines the following default mappings between CLX modifiers and
key-event modifiers:
@itemize
@item
@f{(xlib:make-state-mask :mod-1)    -->  "Meta"}

@item
@f{(xlib:make-state-mask :control)  -->  "Control"}

@item
@f{(xlib:make-state-mask :lock)     -->  "Lock"}

@item
@f{(xlib:make-state-mask :shift)    -->  "Shift"}
@end itemize
@end defun


@defun {make-key-event-bits} {@rest{} modifier-names}
This function returns bits suitable for @f{make-key-event} from the supplied
@i{modifier-names}.  If any name is undefined, this signals an error.
@end defun

@defun {key-event-modifier-mask} {modifier-name}
This function returns a mask for @i{modifier-name}.  This mask is suitable for
use with @f{key-event-bits}.  If @i{modifier-name} is undefined, this signals
an error.
@end defun

@defun {key-event-bits-modifiers} {bits}
This returns a list of key-event modifier names, one for each modifier
set in @i{bits}.
@end defun


@defun {translate-key-event} {display scan-code bits}
This function translates the X @i{scan-code} and X @i{bits} to a key-event.
First this maps @i{scan-code} to an X keysym using @f{xlib:keycode->keysym}
looking at @i{bits} and supplying index as @f{1} if the X shift bit is on,
@f{0} otherwise.

If the resulting keysym is undefined, and it is not a modifier keysym,
then this signals an error.  If the keysym is a modifier key, then this
returns @nil{}.

If these conditions are satisfied
@itemize
@item
The keysym is defined.

@item
The X shift bit is off.

@item
The X lock bit is on.

@item
The X keysym represents a lowercase letter.
@end itemize
then this maps the @i{scan-code} again supplying index as @f{1} this time,
treating the X lock bit as a caps-lock bit.  If this results in an undefined
keysym, this signals an error.  Otherwise, this makes a key-event with the
keysym and bits formed by mapping the X bits to key-event bits.

Otherwise, this makes a key-event with the keysym and bits formed by
mapping the X bits to key-event bits.
@end defun


@defun {translate-mouse-key-event} {scan-code bits event-key}
This function translates the X button code, @i{scan-code}, and modifier bits,
@i{bits}, for the X @i{event-key} into a key-event.  See
@f{define-mouse-keysym}.
@end defun

@defun {make-key-event} {object bits}
This function returns a key-event described by @i{object} with @i{bits}.
@i{Object} is one of keysym, string, or key-event.  When @i{object} is a
key-event, this uses @f{key-event-keysym}.  You can form @i{bits} with
@f{make-key-event-bits} or @f{key-event-modifier-mask}.
@end defun

@defun {key-event-p} {object}
This function returns whether @i{object} is a key-event.
@end defun

@defun {key-event-bits} {key-event}
This function returns the bits field of a @i{key-event}.
@end defun

@defun {key-event-keysym} {key-event}
This function returns the keysym field of a @i{key-event}.
@end defun

@defun {char-key-event} {character}
This function returns the key-event associated with @i{character}.  You can
associate a key-event with a character by @f{setf}'ing this form.
@end defun

@defun {key-event-char} {key-event}
This function returns the character associated with @i{key-event}.  You can
associate a character with a key-event by @f{setf}'ing this form.  The system
defaultly translates key-events in some implementation dependent way for text
insertion; for example, under an ASCII system, the key-event @f{#k"C-h"}, as
well as @f{#k"backspace"} would map to the Common Lisp character that causes a
backspace.
@end defun

@defun {key-event-bit-p} {key-event bit-name}
This function returns whether @i{key-event} has the bit set named by
@i{bit-name}.  This signals an error if @i{bit-name} is undefined.
@end defun

@defmac {do-alpha-key-events} {(var kind @optional{} result) @mstar{form}}
 This macro evaluates each @i{form} with @i{var} bound to a key-event
representing an alphabetic character.  @i{Kind} is one of @kwd{lower},
@kwd{upper}, or @kwd{both}, and this binds @i{var} to each key-event in order
as specified in @i{X Window System Protocol, MIT X Consortium Standard, X
Version 11, Release 4}.  When @kwd{both} is specified, this processes lowercase
letters first.
@end defmac

@defun {print-pretty-key} {key @optional{} stream long-names-p}
This prints @i{key}, a key-event or vector of key-events, in a user-expected
fashion to @i{stream}.  @i{Long-names-p} indicates whether modifiers should
print with their long or short name.  @i{Stream} defaults to
@var{standard-output}.
@end defun

@defun {print-pretty-key-event} {key-event @optional{} stream long-names-p}
This prints @i{key-event} to @i{stream} in a user-expected fashion.
@i{Long-names-p} indicates whether modifier names should appear using the long
name or short name.  @i{Stream} defaults to @var{standard-output}.
@end defun



@node CLX Interface, Slave Lisps, Key-events, Auxiliary Systems
@section CLX Interface

@menu
* Graphics Window Hooks::
* Entering and Leaving Windows::
* How to Lose Up-Events::
@end menu

@node Graphics Window Hooks, Entering and Leaving Windows, CLX Interface, CLX Interface
@subsection Graphics Window Hooks
This section describes a few hooks used by Hemlock's internals to handle
graphics windows that manifest Hemlock windows.  Some heavy users of Hemlock as
a tool have needed these in the past, but typically functions that replace the
default values of these hooks must be written in the "@f{HEMLOCK-INTERNALS}"
package.  All of these symbols are internal to this package.

If you need this level of control for your application, consult the current
implementation for code fragments that will be useful in correctly writing your
own window hook functions.

@defvr {Variable} *create-window-hook*
This holds a function that @Hemlock{} calls when @f{make-window} executes under
CLX.  @Hemlock{} passes the CLX display and the following arguments from
@f{make-window}: starting mark, ask-user, x, y, width, height, and modelinep.
The function returns a CLX window or nil indicating one could not be made.
@end defvr

@defvr {Variable} *delete-window-hook*
This holds a function that @hemlock{} calls when @f{delete-window} executes under
CLX.  @hemlock{} passes the CLX window and the @hemlock{} window to this function.
@end defvr

@defvr {Variable} *random-typeout-hook*
This holds a function that @hemlock{} calls when random typeout occurs under CLX.
@hemlock{} passes it a @hemlock{} device, a pre-existing CLX window or @nil{}, and
the number of pixels needed to display the number of lines requested in the
@f{with-pop-up-display} form.  It should return a window, and if a new window
is created, then a CLX gcontext must be the second value.
@end defvr

@defvr {Variable} *create-initial-windows-hook*
This holds a function that @hemlock{} calls when it initializes the screen
manager and makes the first windows, typically windows for the @hid{Main} and
@hid{Echo Area} buffers.  @hemlock{} passes the function a @hemlock{} device.
@end defvr


@node Entering and Leaving Windows, How to Lose Up-Events, Graphics Window Hooks, CLX Interface
@subsection Entering and Leaving Windows

@defvr {Hemlock Variable} {Enter Window Hook}
When the mouse enters an editor window, @hemlock{} invokes the functions in this
hook.  These functions take a @Hemlock{} window as an argument.
@end defvr

@defvr {Hemlock Variable} {Exit Window Hook}
When the mouse exits an editor window, @hemlock{} invokes the functions in this
hook.  These functions take a @Hemlock{} window as an argument.
@end defvr


@node How to Lose Up-Events,  , Entering and Leaving Windows, CLX Interface
@subsection How to Lose Up-Events
Often the only useful activity user's design for the mouse is to click on
something.  @Hemlock{} sees a character representing the down event, but what do
you do with the up event character that you know must follow?  Having the
command eat it would be tasteless, and would inhibit later customizations that
make use of it, possibly adding on to the down click command's functionality.
Bind the corresponding up character to the command described here.

@deffn {Command} {Do Nothing}
This does nothing as many times as you tell it.
@end deffn


@node Slave Lisps, Spelling, CLX Interface, Auxiliary Systems
@section Slave Lisps
@cindex Slave lisp interface functions
Some implementations of @hemlock{} feature the ability to manage multiple slave
Lisps, each connected to one editor Lisp.  The routines discussed here spawn
slaves, send evaluation and compilation requests, return the current server,
etc.  This is very powerful because without it you can lose your editing state
when code you are developing causes a fatal error in Lisp.

The routines described in this section are best suited for creating editor
commands that interact with slave Lisps, but in the past users implemented
several independent Lisps as nodes communicating via these functions.  There is
a better level on which to write such code that avoids the extra effort these
routines take for the editor's sake.  See the @i{CMU Common Lisp User's Manual}
for the @f{remote} and @f{wire} packages.


@menu
* The Current Slave::
* Asynchronous Operation Queuing::
* Synchronous Operation Queuing::
@end menu

@node The Current Slave, Asynchronous Operation Queuing, Slave Lisps, Slave Lisps
@subsection The Current Slave
There is a slave-information structure that these return which is suitable for
passing to the routines described in the following subsections.

@defun {create-slave} {@optional{} name}
This creates a slave that tries to connect to the editor.  When the slave
connects to the editor, this returns a slave-information structure, and the
interactive buffer is the buffer named @i{name}.  This generates a name if
@i{name} is @nil{}.  In case the slave never connects, this will eventually
timeout and signal an editor-error.
@end defun

@defvr {Hemlock Variable} {Current Eval Server}
@end defvr
@defun {get-current-eval-server} {@optional{} errorp}
This returns the server-information for the @hid{Current Eval Server} after
making sure it is valid.  Of course, a slave Lisp can die at anytime.  If this
variable is @nil{}, and @i{errorp} is non-@nil{}, then this signals an
editor-error; otherwise, it tries to make a new slave.  If there is no current
eval server, then this tries to make a new slave, prompting the user based on a
few variables (see the @i{Hemlock User's Manual}).
@end defun

@defvr {Hemlock Variable} {Current Compile Server}
@end defvr
@defun {get-current-compile-server}
This returns the server-information for the @hid{Current Compile Server} after
making sure it is valid.  This may return nil.  Since multiple slaves may
exist, it is convenient to use one for developing code and one for compiling
files.  The compilation commands that use slave Lisps prefer to use the current
compile server but will fall back on the current eval server when necessary.
Typically, users only have separate compile servers when the slave Lisp can
live on a separate workstation to save cycles on the editor machine, and the
@hemlock{} commands only use this for compiling files.
@end defun


@node Asynchronous Operation Queuing, Synchronous Operation Queuing, The Current Slave, Slave Lisps
@subsection Asynchronous Operation Queuing
The routines in this section queue requests with an eval server.  Requests are
always satisfied in order, but these do not wait for notification that the
operation actually happened.  Because of this, the user can continue editing
while his evaluation or compilation occurs.  Note, these usually execute in the
slave immediately, but if the interactive buffer connected to the slave is
waiting for a form to return a value, the operation requested must wait until
the slave is free again.

@defun {string-eval} {string} @mkeys{:server :package :context}
@defunx {region-eval} {region} @mkeys{:server :package :context}
@defunx {region-compile} {region} @mkeys{:server :package}
@f{string-eval} queues the evaluation of the form read from @i{string} on eval
server @i{server}.  @i{Server} defaults to the result of
@f{get-current-server}, and @i{string} is a simple-string.  The evaluation
occurs with @var{package} bound in the slave to the package named by
@i{package}, which defaults to @hid{Current Package} or the empty string; the
empty string indicates that the slave should evaluate the form in its current
package.  The slave reads the form in @i{string} within this context as well.
@i{Context} is a string to use when reporting start and end notifications in
the @hid{Echo Area} buffer; it defaults to the concatenation of @f{"evaluation
of "} and @i{string}.

@f{region-eval} is the same as @f{string-eval}, but @i{context} defaults
differently.  If the user leaves this unsupplied, then it becomes a string
involving part of the first line of region.

@f{region-compile} is the same as the above.  @i{Server} defaults the same; it
does not default to @f{get-current-compile-server} since this compiles the
region into the slave Lisp's environment, to affect what you are currently
working on.
@end defun

@defvr {Hemlock Variable} {Remote Compile File} @val{nil}
@end defvr
@defun {file-compile} {file} @mkeys{:output-file :error-file :load :server :package}
This compiles @i{file} in a slave Lisp.  When @i{output-file} is @true{} (the
default), this uses a temporary output file that is publicly writable in case
the client is on another machine, which allows for file systems that do not
permit remote write access.  This renames the temporary file to the appropriate
binary name or deletes it after compilation.  Setting @hid{Remote Compile File}
to @nil{}, inhibits this.  If @i{output-file} is non-@nil{} and not @true{}, then it
is the name of the binary file to write.  The compilation occurs with
@var{package} bound in the slave to the package named by @i{package}, which
defaults to @hid{Current Package} or the empty string; the empty string
indicates that the slave should evaluate the form in its current package.
@i{Error-file} is the file in which to record compiler output, and a @nil{} value
inhibits this file's creation.  @i{Load} indicates whether to load the
resulting binary file, defaults to @nil{}.  @i{Server} defaults to
@f{get-current-compile-server}, but if this returns nil, then @i{server}
defaults to @f{get-current-server}.
@end defun

@node Synchronous Operation Queuing,  , Asynchronous Operation Queuing, Slave Lisps
@subsection Synchronous Operation Queuing
The routines in this section queue requests with an eval server and wait for
confirmation that the evaluation actually occurred.  Because of this, the user
cannot continue editing while the slave executes the request.  Note, these
usually execute in the slave immediately, but if the interactive buffer
connected to the slave is waiting for a form to return a value, the operation
requested must wait until the slave is free again.

@defun {eval-form-in-server} {server-info string @optional{} package}
 This function queues the evaluation of a form in the server associated with
@i{server-info} and waits for the results.  The server @f{read}'s the form from
@i{string} with @var{package} bound to the package named by @i{package}.  This
returns the results from the slave Lisp in a list of string values.  You can
@f{read} from the strings or simply display them depending on the @f{print}'ing
of the evaluation results.

@i{Package} defaults to @hid{Current Package}.  If this is @nil{}, the server
uses the value of @var{package} in the server.

While the slave executes the form, it binds @var{terminal-io} to a stream that
signals errors when read from and dumps output to a bit-bucket.  This prevents
the editor and slave from dead locking by waiting for each other to reply.
@end defun

@defun {eval-form-in-server-1} {server-info string @optional{} package}
 This function calls @f{eval-form-in-server} and @f{read}'s the result in the
first string it returns.  This result must be @f{read}'able in the editor's
Lisp.
@end defun


@node Spelling, File Utilities, Slave Lisps, Auxiliary Systems
@section Spelling
@cindex Spelling checking
@hemlock{} supports spelling checking and correcting commands based on the ITS
Ispell dictionary.  These commands use the following routines which include
adding and deleting entries, reading the Ispell dictionary in a compiled binary
format, reading user dictionary files in a text format, and checking and
correcting possible spellings.

@defun {spell:maybe-read-spell-dictionary}
This reads the default binary Ispell dictionary.  Users must call this before
the following routines will work.
@end defun

@defun {spell:spell-read-dictionary} {filename}
This adds entries to the dictionary from the lines in the file @i{filename}.
Dictionary files contain line oriented records like the following:
@example
entry1/flag1/flag2
entry2
entry3/flag1
@end example
The flags are the Ispell flags indicating which endings are appropriate for the
given entry root, but these are unnecessary for user dictionary files.  You can
consult Ispell documentation if you want to know more about them.
@end defun

@defun {spell:spell-add-entry} {line @optional{} word-end}
This takes a line from a dictionary file, and adds the entry described by
@i{line} to the dictionary.  @i{Word-end} defaults to the position of the first
slash character or the length of the line.  @i{Line} is destructively modified.
@end defun

@defun {spell:spell-remove-entry} {entry}
This removes entry, a simple-string, from the dictionary, so it will be an
unknown word.  This destructively modifies @i{entry}.  If it is a root word,
then all words derived with @i{entry} and its flags will also be deleted.  If
@i{entry} is a word derived from some root word, then the root and any words
derived from it remain known words.
@end defun

@defun {spell:correct-spelling} {word}
This checks the spelling of @i{word} and outputs the results.  If this finds
@i{word} is correctly spelled due to some appropriate suffix on a root, it
generates output indicating this.  If this finds @i{word} as a root entry, it
simply outputs that it found @i{word}.  If this cannot find @i{word} at all,
then it outputs possibly correct close spellings.  This writes to
@var{standard-output}, and it calls @f{maybe-read-spell-dictionary} before
attempting any lookups.
@end defun

@defvr {Constant} {max-entry-length} val {31}
@end defvr
@defun {spell:spell-try-word} {word word-len}
This returns an index into the dictionary if it finds @i{word} or an
appropriate root.  @i{Word-len} must be inclusively in the range 2 through
@f{max-entry-length}, and it is the length of @i{word}.  @i{Word} must be
uppercase.  This returns a second value indicating whether it found @i{word}
due to a suffix flag, @nil{} if @i{word} is a root entry.
@end defun

@defun {spell:spell-root-word} {index}
This returns a copy of the root word at dictionary entry @i{index}.  This index
is the same as returned by @f{spell-try-word}.
@end defun

@defun {spell:spell-collect-close-words} {word}
This returns a list of words correctly spelled that are @i{close} to @i{word}.
@i{Word} must be uppercase, and its length must be inclusively in the range 2
through @f{max-entry-length}.  Close words are determined by the Ispell rules:
@enumerate
@item
Two adjacent letters can be transposed to form a correct spelling.

@item
One letter can be changed to form a correct spelling.

@item
One letter can be added to form a correct spelling.

@item
One letter can be removed to form a correct spelling.
@end enumerate
@end defun

@defun {spell:spell-root-flags} {index}
This returns a list of suffix flags as capital letters that apply to the
dictionary root entry at @i{index}.  This index is the same as returned by
@f{spell-try-word}.
@end defun


@node File Utilities, Beeping, Spelling, Auxiliary Systems
@section File Utilities
Some implementations of @hemlock{} provide extensive directory editing commands,
@hid{Dired}, including a single wildcard feature.  An asterisk denotes a
wildcard.

@defun {dired:copy-file} {spec1 spec2} @mkeys{:update :clobber: directory}
 This function copies @i{spec1} to @i{spec2}.  It accepts a single wildcard in
the filename portion of the specification, and it accepts directories.  This
copies files maintaining the source's write date.

If @i{spec1} and @i{spec2} are both directories, this recursively copies the
files and subdirectory structure of @i{spec1}; if @i{spec2} is in the
subdirectory structure of @i{spec1}, the recursion will not descend into it.
Use @f{"/spec1/*"} to copy only the files from @i{spec1} to directory
@i{spec2}.

If @i{spec2} is a directory, and @i{spec1} is a file, then this copies
@i{spec1} into @i{spec2} with the same @f{pathname-name}.

When @kwd{update} is non-@nil{}, then the copying process only copies files if the
source is newer than the destination.

When @kwd{update} and @kwd{clobber} are @nil{}, and the destination exists, the
copying process stops and asks the user whether the destination should be
overwritten.

When the user supplies @kwd{directory}, it is a list of pathnames, directories
excluded, and @i{spec1} is a pattern containing one wildcard.  This then copies
each of the pathnames whose @f{pathname-name} matches the pattern.  @i{Spec2}
is either a directory or a pathname whose @f{pathname-name} contains a
wildcard.
@end defun

@defun {dired:rename-file} {spec1 spec2} @mkeys{:clobber :directory}
 This function renames @i{spec1} to @i{spec2}.  It accepts a single wildcard in
the filename portion of the specification, and @i{spec2} may be a directory
with the destination specification resulting in the merging of @i{spec2} with
@i{spec1}.  If @kwd{clobber} is @nil{}, and @i{spec2} exists, then this asks the
user to confirm the renaming.  When renaming a directory, end the specification
without the trailing slash.

When the user supplies @kwd{directory}, it is a list of pathnames, directories
excluded, and @i{spec1} is a pattern containing one wildcard.  This then copies
each of the pathnames whose @f{pathname-name} matches the pattern.  @i{Spec2}
is either a directory or a pathname whose @f{pathname-name} contains a
wildcard.
@end defun

@defun {dired:delete-file} {spec} @mkeys{:recursive :clobber}
 This function deletes @i{spec}.  It accepts a single wildcard in the filename
portion of the specification, and it asks for confirmation on each file if
@kwd{clobber} is @nil{}.  If @kwd{recursive} is non-@nil{}, then @i{spec} may be a
directory to recursively delete the entirety of the directory and its
subdirectory structure.  An empty directory may be specified without
@kwd{recursive} being non-@nil{}.  Specify directories with the trailing
slash.
@end defun

@defun {dired:find-file} {name @optional{} directory find-all}
 This function finds the file with @f{file-namestring} @i{name}, recursively
looking in @i{directory}.  If @i{find-all} is non-@nil{} (defaults to @nil{}), then
this continues searching even after finding a first occurrence of file.
@i{Name} may contain a single wildcard, which causes @i{find-all} to default to
@true{} instead of @nil{}.
@end defun

@defun {package:make-directory} {name}
This function creates the directory with @i{name}.  If it already exists, this
signals an error.
@end defun

@defun {dired:pathnames-from-pattern} {pattern files}
This function returns a list of pathnames from the list @i{files} whose
@f{file-namestring}'s match @i{pattern}.  @i{Pattern} must be a non-empty
string and contain only one asterisk.  @i{Files} contains no directories.
@end defun

@defvr {Variable} dired:*update-default*
@defvrx {Variable} {dired:*clobber-default*}
@defvrx {Variable} {dired:*recursive-default*}
These are the default values for the keyword arguments above with corresponding
names.  These default to @nil{}, @true{}, and @nil{} respectively.
@end defvr

@defvr {Variable} dired:*report-function*
@defvrx {Variable} dired:*error-function*
@defvrx {Variable} dired:*yesp-function*
These are the function the above routines call to report progress, signal
errors, and prompt for @i{yes} or @i{no}.  These all take format strings and
arguments.
@end defvr


@defun {merge-relative-pathnames} {pathname default-directory}
This function merges @i{pathname} with @i{default-directory}.  If @i{pathname}
is not absolute, this assumes it is relative to @i{default-directory}.  The
result is always a directory pathname.
@end defun

@defun {directoryp} {pathname}
This function returns whether @i{pathname} names a directory: it has no name
and no type fields.
@end defun


@node Beeping,  , File Utilities, Auxiliary Systems
@section Beeping

@defun {hemlock-beep}
@Hemlock{} binds @f{system:*beep-function*} to this function to beep the device.
It is different for different devices.
@end defun

@defvr {Hemlock Variable} {Bell Style} @val{:border-flash}
@defvrx {Hemlock Variable} {Beep Border Width} @val{20}
@hid{Bell Style} determines what @var{hemlock-beep} does in @hemlock{} under CLX.
Acceptable values are @kwd{border-flash}, @kwd{feep},
@kwd{border-flash-and-feep}, @kwd{flash}, @kwd{flash-and-feep}, and @nil{} (do
nothing).

@hid{Beep Border Width} is the width in pixels of the border flashed by border
flash beep styles.
@end defvr
